@ECHO OFF

SET SCRIPT="example.ps1"
%WINDIR%\SysWoW64\cmd.exe /K powershell -executionpolicy bypass -File %SCRIPT%

REM If a network has powershell.exe blocked via teh path c:\windows\system32\powershell.exe (and powershell_ise.exe)
REM This will only block powershell with 64-bit bineries, 32-bit bineries can still be used
REM Calling the WOW64 subsystem will open powershell with 32-bit binaries.

REM The lines above shows how you can call a ps1 file in this case
 